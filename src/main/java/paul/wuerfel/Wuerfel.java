package paul.wuerfel;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Wuerfel {

    public static void Rolldice(int erlaubte_seitenzahl, int minimale_seitenzahl) throws InterruptedException{
        minimale_seitenzahl--;
        System.out.print("\033[H\033[2J");
        System.out.flush();
        boolean restart = true;
        boolean wiederholung;
        boolean trysame;
        while (restart == true) {
            restart = false;
            trysame = true;
            try {
                Scanner eingabe = new Scanner(System.in);
                System.out.print("Wie viele seiten hat der Würfel? ");
                int seiten = eingabe.nextInt();
                if (seiten % erlaubte_seitenzahl == 0) {
                    if (seiten > minimale_seitenzahl) {
                        while (trysame == true) {
                            trysame = false;
                            System.out.println("Der Würfel ist auf " + (int) (Math.random() * seiten + 1) + " Gelandet");
                            Scanner scanner = new Scanner(System.in);
                            System.out.println();
                            System.out.print("Nochmahl? drücke J um eine neue zahl einzugeben \noder Enter um die selbe Nummer noch einmal zu nehmen: ");
                            String wiederholen = scanner.nextLine();
                            if (wiederholen.isEmpty()) {
                                trysame = true;
                                System.out.print("\033[H\033[2J");
                                System.out.flush();
                            } else {
                                wiederholung = nochmal(wiederholen);


                                if (wiederholung == true) {
                                    restart = true;
                                    TimeUnit.MICROSECONDS.sleep(500);
                                    System.out.print("\033[H\033[2J");
                                    System.out.flush();
                                } else
                                    return;
                            }
                        }
                    } else {
                        minimale_seitenzahl++;
                        System.out.println("Ein Würfel hat nicht weniger als " + minimale_seitenzahl + " Seiten!");
                        minimale_seitenzahl--;
                        restart = true;
                        clearScreen();
                    }
                } else {
                    System.out.println("Ein würfel hat nur gerade zahlen");
                    restart = true;
                    clearScreen();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (InputMismatchException r) {
                System.out.println("Eingabe muss eine zahl sein!");
                restart = true;
                clearScreen();
            } catch (ArithmeticException uff) {
                System.out.println("Ein fehler in der config datei ist aufgetreten lade standart werte");

                erlaubte_seitenzahl = 2;
                minimale_seitenzahl = 6;
                restart = true;
                clearScreen();
            }
        }
    }


    public static boolean nochmal(String x) {
        return x.charAt(0) == 'J' || x.charAt(0) == 'j';
    }

    public static void clearScreen() throws InterruptedException {
        System.out.println();
        System.out.print("Neustart in 3");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("\b2");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("\b1");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }


}
