package paul.wuerfel;

import java.util.Scanner;

public class Config {

    public static void Conf() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
        Scanner input = new Scanner(System.in);
        System.out.print("soll der Würfel ungerade zahlen haben [J/n]: ");
        String ungerade = input.next();
        ungerade = ungerade.toLowerCase();
        int erlaubte_seitenzahl;
        if (!ungerade.equals("j")){
            erlaubte_seitenzahl = 1;
        } else {
            erlaubte_seitenzahl = 2;
        }
        System.out.print("\033[H\033[2J");
        System.out.flush();
        System.out.print("Was soll der kleinste würfel sein: ");
        int minimale_seitenzahl = input.nextInt();
        try {
            Wuerfel.Rolldice(erlaubte_seitenzahl, minimale_seitenzahl);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}







