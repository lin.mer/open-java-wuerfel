
import paul.wuerfel.Config;
import paul.wuerfel.Wuerfel;

public class Start {
    public static void main(String[] args) throws InterruptedException {
        int minimale_seitenzahl = 6;
        int erlaubte_seitenzahl = 2;
        String longargument = "--config";
        String shortargument ="-c";
        String help = "--help";
        for (String arg : args) {
            if (arg.matches(longargument)) {
                Config.Conf();
                return;
            } else if (arg.matches(shortargument)) {
                Config.Conf();
                return;
            } else if (arg.matches(help)) {
                System.out.println("Run --config or -c to configure");
                return;
            } else {
                throw new IllegalArgumentException();
            }
        }
        Wuerfel.Rolldice(erlaubte_seitenzahl,minimale_seitenzahl);
    }
}
